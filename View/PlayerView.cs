﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View
{
    public class PlayerView
    {
        public int id { get; set; }
        public string name { get; set; }
        public string surname { get; set; }
        public int? goals { get; set; }
        public int teamId { get; set; }        
        public string team { get; set; }
        public string position { get; set; }
        public int matchesHome { get; set; }
        public int matchesOut { get; set; }
    }
}
