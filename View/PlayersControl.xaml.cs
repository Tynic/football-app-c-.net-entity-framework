﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Console;
using System.ComponentModel;

namespace View
{
    /// <summary>
    /// Interaction logic for PlayersControl.xaml
    /// </summary>
    public partial class PlayersControl : UserControl
    {
        Manager manager;

        public ICollectionView ViewSource { get; set; }

        public PlayersControl()
        {
            InitializeComponent();
        }
        public void Init(Manager manager)
        {
            this.manager = manager;
            // what will be displayed in Player "bookmark"
            //playerDataGrid.ItemsSource = manager.GetPlayerData();
            // Create CollectionView based on the data you want to show
            // var playerRoasterMatchCC = manager.playerRoasterMatchCC;
            //playerDataGrid.ItemsSource = CollectionViewSource.GetDefaultView(manager.playerRoasterMatchCC);
            playerDataGrid.ItemsSource = manager.GetPlayerData();
            InitializeComponent();            
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dataGridPlayers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // playerGrid.DataContext = (Player)playerDataGrid.SelectedItem;
             playerGrid.DataContext = playerDataGrid.SelectedItem;
             SetPlayerTxt(false);            
        }

        // Selecting item in DataGrid will send object Person to Grid, where will be displayed in textbox
         public void SetPlayerTxt(bool enabled)
        {
            txtName.IsEnabled = enabled;
            txtSurname.IsEnabled = enabled;
            txtGoals.IsEnabled = enabled;
            txtTeam.IsEnabled = enabled;
            txtPosition.IsEnabled = enabled;
            txtMatchesHome.IsEnabled = enabled;
            txtMatchesOut.IsEnabled = enabled;
        }

        /// <summary>
        /// Reload grid so changes in database will be visible
        /// </summary>
        public void reloadPlayersDataGrid()
        {
            playerDataGrid.Items.Refresh();
            //playerDataGrid.ItemsSource = null;
            //playerDataGrid.ItemsSource = manager.GetPlayerData();
        }

        private void playerDataGrid_Selected(object sender, RoutedEventArgs e)
        {

        }

        /// <summary>
        /// Reload datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReload(object sender, RoutedEventArgs e)
        {
            playerDataGrid.ItemsSource = null;
            playerDataGrid.ItemsSource = manager.GetPlayerData();
        }
    }
}
