﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Console;
using System.Collections;
using View.Properties;

namespace View
{
    /// <summary>
    /// Interaction logic for PickPlayers.xaml
    /// </summary>
    public partial class PickPlayers : Window
    {
        Manager manager;
        String teamName;
        public bool done;

        // Lists for storing home team and guest team players
        private List<Pl_Checkbox> selectedPlayersList; //= new List<Pl_Checkbox>();

        public PickPlayers(Manager manager, String teamName, List<Pl_Checkbox> selectedPlayersList)
        {
            InitializeComponent();
            this.manager = manager;
            this.teamName = teamName;
            var teamList = manager.GetTeamPlayers(teamName);
            pickPlayersDataGrid.ItemsSource = teamList;            
            done_button.IsEnabled = false;
            this.done = false;
            this.selectedPlayersList = selectedPlayersList;
        }

        private void done_button_Click(object sender, RoutedEventArgs e)
        {
            done = true;
            Close();
        }

        /// <summary>
        /// Gets selected players and inserts them to list
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Players_Checked(object sender, RoutedEventArgs e)
        {                        
            var pl = (Player)(e.Source as CheckBox).DataContext;                 

            Pl_Checkbox tmp = new Pl_Checkbox();
            tmp.id = pl.id;
            tmp.name = pl.name;
            tmp.position1 = manager.GetPositionStr(pl.position_fk);
            tmp.surname = pl.surname;
            tmp.team_name_id = pl.team_fk;

            AddPlayerToList(selectedPlayersList, tmp);
        }

        private void AddPlayerToList(List<Pl_Checkbox> selectedPlayersList, Pl_Checkbox checkedPlayer)
        {
            selectedPlayersList.Add(checkedPlayer);
            checkPlayers();
        }

        /// <summary>
        /// Checks if selected players has one goal keeper and 10 players       
        /// </summary>
        private void checkPlayers()
        {
            int goal_keeper = 0;
            int players = 0;

            foreach (var item in selectedPlayersList)
            {
                if (item.position1 == "Goalkeeper")
                {
                    goal_keeper++;
                }
                else
                {
                    players++;
                }
            }
            CheckIfReady(goal_keeper, players);
        }

        /// <summary>
        /// Compares selected players and goal keeper(s) and generates 
        /// additional message in string to send it to statusbar
        /// </summary>
        /// <param name="goalKeeper">How many goal keepers are selected by user</param>
        /// <param name="players">How many players are selected by user</param>

        private void CheckIfReady(int goalKeeper, int players)
        {
            string strPlayers = null;
            string strGoalKeeper = null;
            int missPlayers;
            int diffGoalKeper;

            if (goalKeeper == 1 && players == 10)
            {
                done_button.IsEnabled = true;

                statusbar_pick_players_messages.Text = "You can start a game! ";
                return;
            }

            if (goalKeeper < 1)
            {
                strGoalKeeper = "Select 1 goalkeeper. ";
            }

            if (players < 10)
            {
                missPlayers = 10 - players;
                strPlayers = "Select " + missPlayers;
                if (missPlayers > 1)
                {
                    strPlayers += " players. ";
                }
                else
                {
                    strPlayers += " player. ";
                }
            }

            if (players > 10)
            {
                missPlayers = players - 10;
                strPlayers = "Unselect " + missPlayers;
                if (missPlayers > 1)
                {
                    strPlayers += " players. ";
                }
                else
                {
                    strPlayers += " player. ";
                }
            }

            if (goalKeeper > 1)
            {
                diffGoalKeper = goalKeeper - 1;
                strGoalKeeper = "Unselect " + diffGoalKeper;
                if (diffGoalKeper > 1)
                {
                    strGoalKeeper += " goal keepers. ";
                }
                else
                {
                    strGoalKeeper += " goal keeper. ";
                }
            }
            
            statusbar_pick_players_messages.Text = strGoalKeeper + strPlayers;
            done_button.IsEnabled = false;


        }

        /// <summary>
        /// If user unselect player, it founds his id
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Players_Unchecked(object sender, RoutedEventArgs e)
        {
            var pl = (Player)(e.Source as CheckBox).DataContext;

            int index = pl.id;

            DeleteFromList(index);
            //SelectedPlayers.Remove((e.OriginalSource as CheckBox).Name.ToString());
        }

        /// <summary>
        /// Removes player from list by id
        /// </summary>
        /// <param name="index">Id of unselected player</param>
        private void DeleteFromList(int index)
        { 
            foreach (var item in selectedPlayersList)
            {
                if (item.id == index)
                {
                    selectedPlayersList.Remove(item);
                    break;
                }
            }
            checkPlayers();
        }
    }
}
