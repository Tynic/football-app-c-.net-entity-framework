﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View
{
    public class TeamView
    {
        public int id { get; set; }
        public int order { get; set; }
        public string name { get; set; }
        public int? points { get; set; }
        public int matchesHome { get; set; }
        public int matchesOut { get; set; }
        public string type { get; set; }

    }
}
