﻿using Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for PlayersControl.xaml
    /// </summary>
    public partial class MatchesControl : UserControl
    {
        Manager manager;
        public MatchesControl()
        {
            InitializeComponent();
        }
        public void Init(Manager manager)
        {
            this.manager = manager;
            matchDataGrid.ItemsSource = manager.GetMatchData();
            SetMatchesButtons(true, false, false);
        }

        private void SetMatchesButtons(bool newMatch, bool shoot, bool save)
        {
            btnNew.IsEnabled = newMatch;
            btnShoot.IsEnabled = shoot;
            btnSave.IsEnabled = save;
        }

        private void btnNew_Click(object sender, RoutedEventArgs e)
        {
            //manager.NewMatch();
            manager.New();

            // reload the datasource of datagrid after the update so new row appears
            reloadMatchDataGrid();
            SetMatchesButtons(false, false, true);
        }

        /// <summary>
        /// Pressing shoot button method
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnShootCLick(object sender, RoutedEventArgs e)
        {
            //get selected match id and show window
            var matchSelected = (MatchView)matchDataGrid.SelectedItem;

            Shoot newWindow = new Shoot(manager, matchSelected.id);
            newWindow.ShowDialog();

            var selIndex = matchDataGrid.SelectedIndex;

            // reload the datasource of datagrid after the update so new row appears
            reloadMatchDataGrid();
            matchDataGrid.SelectedIndex = selIndex;
            SetMatchesButtons(true, true, true);            
        }


        // Reload matchGrid so changes in database will be visible
        private void reloadMatchDataGrid()
        {
            matchDataGrid.Items.Refresh();
            //matchDataGrid.ItemsSource = null;
            matchDataGrid.ItemsSource = manager.GetMatchData();
        }

        private void ComboBox_Teams_Loaded(object sender, RoutedEventArgs e)
        {
            // get the comboBox reference.
            var comboBox = sender as ComboBox;

            comboBox.ItemsSource = manager.GetTeamList();

            // make the first item selected.
            comboBox.SelectedIndex = 0;
        }

        private void ComboBox_Teams_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {

        }

        private void matchSelected(object sender, RoutedEventArgs e)
        {

        }

        private void dataGridMatches_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var tmp = (MatchView)matchDataGrid.SelectedItem;
            // to show selected match values in textboxes
            matchGrid.DataContext = tmp;

            // if game has NOT ended yet
            if (tmp == null || tmp.endTime == null)
            {
                SetMatchesButtons(true, true, true);
            }
            else //if the game ended
            {
                SetMatchesButtons(true, false, false);
            }
        }

        /// <summary>
        /// When match is ended it calls update method to insert end time
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnEndMatchClick(object sender, RoutedEventArgs e)
        {
            var matchEnd = (MatchView)matchDataGrid.SelectedItem;
            var selIndex = matchDataGrid.SelectedIndex;

            // inserts end time
            manager.endMatch(matchEnd);

            //TODO add points
            manager.addPoint(matchEnd);

            SetMatchesButtons(true, false, false);

            reloadMatchDataGrid();

            matchDataGrid.SelectedIndex = selIndex;
        }
    }
}
