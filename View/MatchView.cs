﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View
{
    public class MatchView
    {
        public int id { get; set; }
        public string homeTeam { get; set; }
        public int homeTeamId { get; set; }
        public int? homeTeamScore { get; set; }
        public string guestTeam { get; set; }
        public int guestTeamId { get; set; }
        public int? guestTeamScore { get; set; }
        public string state { get; set; }
        public DateTime? startTime { get; set; }
        public DateTime? endTime { get; set; }
    }
}
