﻿using Console;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for NewMatchWindow.xaml
    /// </summary>
    public partial class NewMatchWindow : Window
    {
        Manager manager;
        public string homeTeamNameStr = null;
        public string guestTeamNameStr = null;

        Team homeTeam;
        Team guestTeam;

        public int selectedTeamId = -1;
        int homeTeamId;
        int guestTeamId;

        public bool selectChanged = true;

        // Lists for storing home team and guest team players
        private List<Pl_Checkbox> homeTeamList;
        private List<Pl_Checkbox> guestTeamList;

        public NewMatchWindow(Manager manager, List<Pl_Checkbox> homeTeamList, List<Pl_Checkbox> guestTeamList)
        {
            InitializeComponent();
            this.manager = manager;

            this.homeTeamList = homeTeamList;
            this.guestTeamList = guestTeamList;

            Start_Game_Button.IsEnabled = false;            
        }

        private void ComboBoxLoadedTeam(object sender, RoutedEventArgs e)
        {
            // get the comboBox reference.
            var comboBox = sender as ComboBox;

            // combobox will show team list
            comboBox.ItemsSource = manager.GetTeamList();
            
        }

        /// <summary>
        /// When user selects team, window with it's players occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboboxSelectedHomeTeam(object sender, SelectionChangedEventArgs e)
        {
            selectChanged = true;
            homeTeamList.Clear();
            var comboBox = sender as ComboBox;
            var selItem = (Team)(Home_Team_Combobox.SelectedItem);
            if (selItem != null)
            {
                newWindowHomeOpen(selItem);
            }
        }

        /// <summary>
        /// Called when no change wasn't made in combobox in case the same value was selected like before
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void HomeTeamCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (selectChanged == false)// &&homeTeamNameStr == newTeamStr)
            {
                var comboBox = sender as ComboBox;
                var selItem = (Team)(Home_Team_Combobox.SelectedItem);
                string newTeamStr = selItem.team_name;
                if (selItem != null)
                {
                    newWindowGuestOpen(selItem);
                }
            }
            selectChanged = false;
        }

        /// <summary>
        /// When user selects team, window with it's players occurs.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboboxSelectedGuestTeam(object sender, SelectionChangedEventArgs e)
        {
            selectChanged = true;
            guestTeamList.Clear();
            var comboBox = sender as ComboBox;
            var selItem = (Team)(Guest_Team_Combobox.SelectedItem);
            if (selItem != null)
            {
                newWindowGuestOpen(selItem);
            }
        }


        /// <summary>
        /// Called when no change wasn't made in combobox in case the same value was selected like before
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GuestTeamCombobox_DropDownClosed(object sender, EventArgs e)
        {
            if (selectChanged == false)// &&homeTeamNameStr == newTeamStr)
            {
                var comboBox = sender as ComboBox;
                var selItem = (Team)(Guest_Team_Combobox.SelectedItem);
                string newTeamStr = selItem.team_name;
                if (selItem != null)
                {
                    newWindowGuestOpen(selItem);
                }
            }
            selectChanged = false;
        }

        /// <summary>
        /// Opens new window for picking players
        /// </summary>
        /// <param name="selItem"></param>
        private void newWindowHomeOpen(Team selItem)
        {
            selectedTeamId = selItem.id;
            homeTeamId = selItem.id;
            homeTeam = selItem;
            homeTeamNameStr = selItem.team_name;
            var newWindow = new PickPlayers(manager, homeTeamNameStr, homeTeamList);
            newWindow.ShowDialog();

            if (newWindow.done == false)
            {
                Home_Team_Combobox.SelectedIndex = -1;
                homeTeamList.Clear();
            }
            Enable_Start_Button();
        }

        /// <summary>
        /// Opens new window for picking players
        /// </summary>
        /// <param name="selItem"></param>
        private void newWindowGuestOpen(Team selItem)
        {
            selectedTeamId = selItem.id;
            guestTeamId = selItem.id;
            guestTeam = selItem;
            guestTeamNameStr = selItem.team_name;
            var newWindow = new PickPlayers(manager, guestTeamNameStr, guestTeamList);
            newWindow.ShowDialog();

            if (newWindow.done == false)
            {
                Guest_Team_Combobox.SelectedIndex = -1;
                guestTeamList.Clear();
            }
            Enable_Start_Button();
        }


        /// <summary>
        /// Decide if game can be started
        /// </summary>
        private void Enable_Start_Button()
        {
            string msg = " ";
            if (homeTeamNameStr == guestTeamNameStr)
            {
                msg = "You can't select 2 same teams!";                
            }
            else if (Home_Team_Combobox.SelectedIndex > -1 && Guest_Team_Combobox.SelectedIndex > -1 && manager.checkLeague(homeTeamId, guestTeamId) == true)
            {
                msg = "Teams already played together."; //TODO
            }
            else if (Home_Team_Combobox.SelectedIndex > -1 && Guest_Team_Combobox.SelectedIndex > -1)
            {
                if (homeTeamList.Count() == 11 && guestTeamList.Count() == 11)
                {
                    msg = "You can start a new match!";
                    Start_Game_Button.IsEnabled = true;
                }
            }
            StatusMessage(msg);
        }

        /// <summary>
        /// Sends string to statusar
        /// </summary>
        /// <param name="str"></param>
        private void StatusMessage(string str)
        {
            statusbar_messages.Text = str;
        }

        /// <summary>
        /// Inserts all information to the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StartGameButtonClick(object sender, RoutedEventArgs e)
        {
            manager.NewMatch(homeTeamList, guestTeamList, homeTeam, guestTeam);
            Close();
        }

    }
}
