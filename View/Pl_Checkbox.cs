﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace View
{
    public class Pl_Checkbox
    {
        public int id { get; set; }
        public string name { get; set; }
        public string position1 { get; set; }
        public string surname { get; set; }
        public int team_name_id { get; set; }
    }
}
