﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using Console;
using System.Collections;
using System.Windows.Data;
using System.Windows.Documents;

namespace View
{
    /// <summary>
    /// Manages database data.
    /// It contains ObservableCollection where changes are automatically visible in DataGrid
    /// </summary>
    public class Manager
    {
        FootballConnection db = new FootballConnection();

        // Change in ObservableCollection is automatically shown in PlayerDataGrid
        // PLAYER
        private ObservableCollection<Player> players = new ObservableCollection<Player>();

        public ObservableCollection<Player> Players { get { return players; } }

        // TEAM
        private ObservableCollection<Team> teams = new ObservableCollection<Team>();
        public ObservableCollection<Team> Teams { get { return teams; } }

        // MATCH
        private ObservableCollection<Match> matches = new ObservableCollection<Match>();
        public ObservableCollection<Match> Matches { get { return matches; } }

        // ROSTER
        private ObservableCollection<Roster> rosters = new ObservableCollection<Roster>();
        public ObservableCollection<Roster> Rosters { get { return rosters; } }

        // GOAL
        private ObservableCollection<Goal> goals = new ObservableCollection<Goal>();
        public ObservableCollection<Goal> Goals { get { return goals; } }

        public CompositeCollection playerRoasterMatchCC;

        public IList playerData;
        public IList teamData;
        public IList matchData;

        // Lists for storing home team and guest team players
        private List<Pl_Checkbox> homeTeamList;
        private List<Pl_Checkbox> guestTeamList;


        public Manager()
        {
            this.homeTeamList = new List<Pl_Checkbox>();
            this.guestTeamList = new List<Pl_Checkbox>();

            /**
            this.playerRoasterMatchCC = new CompositeCollection();
            CollectionContainer cont1 = new CollectionContainer() { Collection = Players };
            CollectionContainer cont2 = new CollectionContainer() { Collection = Rosters };
            CollectionContainer cont3 = new CollectionContainer() { Collection = Teams };
            playerRoasterMatchCC.Add(cont1);
            playerRoasterMatchCC.Add(cont2);
            playerRoasterMatchCC.Add(cont3);
            **/
            playerData = GetPlayerData();
            teamData = GetTeamData();
            matchData = GetMatchData();

            db.Players.ToList().ForEach(p => players.Add(p));
            db.Teams.ToList().ForEach(t => teams.Add(t));
            db.Matches.ToList().ForEach(m => matches.Add(m));
        }

        public IList GetRosterData()
        {
            var res = (from ro in db.Rosters
                       select ro).ToList();
            return res;
        }

        /// <summary>
        /// Get player data
        /// </summary>
        /// <returns></returns>
        public IList GetPlayerData()
        {
            var playersList = (from pl in db.Players
                               select new PlayerView()
                               {
                                   id = pl.id,
                                   name = pl.name,
                                   surname = pl.surname,
                                   goals = pl.goals_total,
                                   team = pl.Team.team_name,
                                   teamId = pl.Team.id,
                                   position = pl.Position.position1,
                               }).ToList();

            foreach (var item in playersList)
            {
                PlayerView tmp = matchPlace(item);
                item.matchesHome = tmp.matchesHome;
                item.matchesOut = tmp.matchesOut;
            }
            return playersList;
        }

        /// <summary>
        /// Counts how many times player played on home field or outside
        /// </summary>
        /// <param name="player"></param>
        /// <returns></returns>
        public PlayerView matchPlace(PlayerView player)
        {
            var result = (from pl in db.Players
                          join ro in db.Rosters on pl.id equals ro.player_fk
                          join ma in db.Matches on ro.match_fk equals ma.id
                          join te in db.Teams on ma.home_team_fk equals te.id
                          where (ma.home_team_fk == te.id && ro.player_fk == player.id)
                          select new { playerId = pl.id, teamId = te.id, ma.home_team_fk, ma.guest_team_fk }).ToList();

            int home = 0;
            int outs = 0;

            foreach (var tmp in result)
            {
                if (tmp.home_team_fk == player.teamId)
                {
                    home++;
                }
                if (tmp.guest_team_fk == player.teamId)
                {
                    outs++;
                }
            }
            player.matchesHome = home;
            player.matchesOut = outs;

            return player;
        }

        /// <summary>
        /// How many matches played each player
        /// </summary>
        /// <param name="playerID"></param>
        /// <returns></returns>
        public int playerMatchesCount(int playerID)
        {
            int count = 0;

            foreach (var Roster in db.Rosters)
            {
                if (Roster.player_fk == playerID)
                {
                    count++;
                }
            }
            return count;
        }


        /// <summary>
        /// Get team data
        /// </summary>
        /// <returns></returns>
        public IList GetTeamData()
        {
            var teamView = (from te in db.Teams
                            select new TeamView()
                            {
                                id = te.id,
                                // order = getOrder(te),
                                name = te.team_name,
                                points = te.points,
                                type = null
                            }).ToList();

            foreach (var team in teamView)
            {
                team.order = getOrder(team.id);
                team.matchesHome = matchesHomeCount(team.id);
                team.matchesOut = matchesOutCount(team.id);
            }

            return teamView;
        }

        /// <summary>
        /// Returns order of the team in statistics
        /// </summary>
        /// <param name="teamId">Team's id</param>
        /// <returns>order</returns>
        public int getOrder(int teamId)
        {
            //we will start from 1st
            int order = 1;

            //at first points decide
            var ord = (from te in db.Teams
                       orderby te.points descending
                       select te).ToList();

            foreach (var item in ord)
            {
                if (item.id == teamId)
                {
                    break;
                }
                order++;
            }

            return order;
        }

        /// <summary>
        /// For given team's ID returns total count of played matches
        /// </summary>
        /// <param name="teamID"></param>
        /// <returns>Played matches count</returns>
        public int TeamMatchesCount(int teamID)
        {
            int count = 0;

            foreach (var Match in db.Matches)
            {
                if (Match.guest_team_fk == teamID || Match.home_team_fk == teamID)
                {
                    count++;
                }
            }
            return count;
        }

        /// <summary>
        /// Get matches data
        /// </summary>
        /// <returns></returns>
        public IList GetMatchData()
        {
            var matchesList = (from ma in db.Matches
                               join teh in db.Teams on ma.home_team_fk equals teh.id
                               join tef in db.Teams on ma.guest_team_fk equals tef.id
                               //select new  { ma.home_score, ma.guest_score, home_team = teh.team_name, guest_team = tef.team_name, ma.start_time, ma.end_time}).ToList();
                               select new MatchView()
                               {
                                   id = ma.id,
                                   homeTeam = teh.team_name,
                                   homeTeamId = teh.id,
                                   homeTeamScore = ma.home_score,
                                   guestTeam = tef.team_name,
                                   guestTeamId = tef.id,
                                   guestTeamScore = ma.guest_score,
                                   startTime = ma.start_time,
                                   endTime = ma.end_time,
                                   //state = GetState(ma.end_time)
                               }).ToList();

            foreach (var match in matchesList)
            {
                match.state = GetState(match.endTime);
            }

            return matchesList;
        }
        /// <summary>
        /// Mark as running if match is running or not
        /// </summary>
        /// <param name="endTime">End time of the match. Null if mactch ended</param>
        /// <returns></returns>
        public string GetState(DateTime? endTime)
        {
            string str;

            if (endTime == null)
            {
                str = "Running";
            }
            else
            {
                str = "Ended";
            }
            return str;
        }

        public void New()
        {
            var newWindow = new NewMatchWindow(this, homeTeamList, guestTeamList);
            newWindow.ShowDialog();
        }

        /// <summary>
        /// Creates new match and update the database
        /// </summary>
        /// <param name="homeTeamList"></param>
        /// <param name="guestTeamList"></param>
        public void NewMatch(List<Pl_Checkbox> homeTeamList, List<Pl_Checkbox> guestTeamList, Team homeTeam, Team guestTeam)
        {
            // creates new match for the database update
            Match newMatch = new Match();
            newMatch.home_score = 0;
            newMatch.guest_score = 0;
            newMatch.guest_team_fk = guestTeam.id;
            newMatch.home_team_fk = homeTeam.id;
            newMatch.start_time = DateTime.Now;

            db.Matches.Add(newMatch);
            db.Entry(newMatch).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();

            NewRosters(homeTeamList, newMatch.id);
            NewRosters(guestTeamList, newMatch.id);
        }

        /// <summary>
        /// Generates roster for every player in match
        /// </summary>
        /// <param name="homeTeamList"></param>
        /// <param name="guestTeamList"></param>
        /// <param name=""></param>
        public void NewRosters(List<Pl_Checkbox> teamList, int newMatchId)
        {
            foreach (var item in teamList)
            {
                Roster newRoster = new Roster();
                newRoster.match_fk = newMatchId;
                newRoster.player_fk = item.id;
                db.Rosters.Add(newRoster);
                db.Entry(newRoster).State = System.Data.Entity.EntityState.Added;
                db.SaveChanges();
            }
        }

        public IList GetTeamList()
        {
            var teamNamesList = (from te in db.Teams
                                 select te).ToList();

            return teamNamesList;
        }

        public IList GetTeamPlayers(String teamString)
        {

            var teamPlayersList = (from pl in db.Players
                                   join pos in db.Positions on pl.position_fk equals pos.id
                                   join te in db.Teams on pl.team_fk equals te.id
                                   where te.team_name.Equals(teamString)
                                   select pl).ToList();
            return teamPlayersList;

        }

        public string GetTeamStrName(int index)
        {
            var teamStrName = (from te in db.Teams
                               where te.id.Equals(index)
                               select te).First();

            return teamStrName.team_name;
        }

        public string GetPositionStr(int index)
        {
            var posStr = (from pos in db.Positions
                          where pos.id.Equals(index)
                          select pos).First();

            return posStr.position1;
        }

        public void Shoot()
        {

        }

        /// <summary>
        /// Gets one of three goal types - Goal, Penalty, Own Goal
        /// </summary>
        public IList GetGoalType()
        {
            var goalTypesList = (from ty in db.GoalTypes
                                 select ty).ToList();

            return goalTypesList;
        }

        /// <summary>
        /// Gets all players in match specified by matchId
        /// </summary>
        /// <param name="matchId"></param>
        /// <returns></returns>
        public IList GetGamePlayers(int matchId)
        {
            var gamePlayersList = (from ro in db.Rosters
                                   join pl in db.Players on ro.player_fk equals pl.id
                                   where ro.match_fk.Equals(matchId)
                                   select pl).ToList();
            return gamePlayersList;
        }

        /// <summary>
        /// Update score when player shoots goal
        /// </summary>
        /// <param name="matchId">ID of the current game</param>
        /// <param name="teamId">ID of the player's team</param>
        /// <param name="goalType">Type of the shot goal</param>
        public void updateScore(int matchId, int teamId, string goalType, int playerId)
        {
            Match result = (from ma in db.Matches
                            where (ma.id == matchId)
                            select ma).First();

            Player shootingPlayer = (from pl in db.Players
                                     where pl.id == playerId
                                     select pl).First();

            if (teamId == result.guest_team_fk)
            {
                if (goalType == "Own goal")
                {
                    result.home_score += 1;
                }
                else
                {
                    result.guest_score += 1;
                    shootingPlayer.goals_total += 1;
                }
            }
            else if (teamId == result.home_team_fk)
            {
                if (goalType == "Own goal")
                {
                    result.guest_score += 1;
                }
                else
                {
                    result.home_score += 1;
                    shootingPlayer.goals_total += 1;
                }
            }

            db.SaveChanges();
        }

        /// <summary>
        /// Update endtime and end the match
        /// </summary>
        /// <param name="matchView"></param>
        public void endMatch(MatchView matchView)
        {
            DateTime newTime = matchView.startTime.Value.AddMinutes(90);

            Match result = (from ma in db.Matches
                            where (ma.id == matchView.id)
                            select ma).First();

            result.end_time = newTime;

            db.SaveChanges();
        }

        /// <summary>
        /// Add points to teams after match end
        /// </summary>
        /// <param name="matchView"></param>
        public void addPoint(MatchView matchView)
        {
            Team homeTeam = (from te in db.Teams
                             join ma in db.Matches on te.id equals ma.home_team_fk
                             select te).First();

            Team guestTeam = (from te in db.Teams
                              join ma in db.Matches on te.id equals ma.guest_team_fk
                              select te).First();

            // 3 points to winning team
            if (matchView.homeTeamScore > matchView.guestTeamScore)
            {
                homeTeam.points += 3;
            }
            else if (matchView.homeTeamScore == matchView.guestTeamScore)
            {
                homeTeam.points += 1;
                guestTeam.points += 1;
            }
            else
            {
                guestTeam.points += 3;
            }
            db.SaveChanges();
        }

        /// <summary>
        /// Counts how many times team played on home field
        /// </summary>
        /// <param name="team">Current team</param>
        /// <returns>count</returns>
        public int matchesHomeCount(int teamId)
        {
            var result = (from ma in db.Matches
                          join te in db.Teams on ma.home_team_fk equals te.id
                          where te.id == teamId
                          select te).ToList();

            int count = result.Count();

            return count;
        }

        /// <summary>
        /// Counts how many times team played on guest field (out)
        /// </summary>
        /// <param name="team">Current team</param>
        /// <returns></returns>
        public int matchesOutCount(int teamId)
        {
            var result = (from ma in db.Matches
                          join te in db.Teams on ma.guest_team_fk equals te.id
                          where te.id == teamId
                          select te).ToList();

            int count = result.Count();

            return count;
        }

        /// <summary>
        /// Update Goal entity when player is shooting
        /// </summary>
        /// <param name="player_fk"></param>
        /// <param name="teamId"></param>
        /// <param name="matchId"></param>
        /// <param name="type_fk"></param>
        public void updateGoal(int player_fk, int teamId, int matchId, int type_fk)
        {
            Goal newGoal = new Goal();
            newGoal.time = DateTime.Now;
            newGoal.player_fk = player_fk;
            newGoal.match_fk = matchId;
            newGoal.goal_type_fk = type_fk;

            db.Goals.Add(newGoal);
            db.Entry(newGoal).State = System.Data.Entity.EntityState.Added;
            db.SaveChanges();
        }

        // LEAGUE
        public bool checkLeague(int homeId, int guestId)
        {
            var result = (from ma in db.Matches
                          select ma).ToList();

            foreach (var match in result)
            {
                if (match.home_team_fk == homeId && match.guest_team_fk == guestId)
                {
                    return true; // teams already played together
                }
                else if (match.home_team_fk == guestId && match.guest_team_fk == homeId)
                {
                    return true;
                }
            }
            return false;
        }
    }
}

