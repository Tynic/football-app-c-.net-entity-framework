﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace View
{
    /// <summary>
    /// Interaction logic for TeamsControl.xaml
    /// </summary>
    public partial class TeamsControl : UserControl
    {
        Manager manager;
        public TeamsControl()
        {
            InitializeComponent();
        }
        public void Init(Manager manager)
        {
            this.manager = manager;

            // set data source for Team listing
            teamDataGrid.ItemsSource = manager.GetTeamData();
        }

        private void dataGridTeams_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            // playerGrid.DataContext = (Player)playerDataGrid.SelectedItem;
            teamGrid.DataContext = teamDataGrid.SelectedItem;
            SetMatchTxt(false);
        }

        // Selecting item in DataGrid will send object Person to Grid, where will be displayed in textbox
        public void SetMatchTxt(bool enabled)
        {
            txtOrder.IsEnabled = enabled;
            txtName.IsEnabled = enabled;
            txtMatchesHome.IsEnabled = enabled;
            txtMatchesOut.IsEnabled = enabled;
            txtPoints.IsEnabled = enabled;
        }

        /// <summary>
        /// Reload datagrid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnReload(object sender, RoutedEventArgs e)
        {
            teamDataGrid.ItemsSource = null;
            teamDataGrid.ItemsSource = manager.GetPlayerData();
        }
    }
}

