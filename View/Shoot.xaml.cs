﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Console;
using System.Collections;
using View.Properties;

namespace View
{
    /// <summary>
    /// Interaction logic for Shoot.xaml
    /// </summary>
    public partial class Shoot : Window
    {
        Manager manager;
        int matchId;
        bool playerChecked;
        public bool done = false;
        Player shootingPlayer;

        public Shoot(Manager manager, int matchId)
        {
            InitializeComponent();
            this.manager = manager;
            this.matchId = matchId;
            // get all players in this match
            var playersList = manager.GetGamePlayers(matchId);
            gamePlayersDataGrid.ItemsSource = playersList;
            // disable combobox until player is selested
            GoalTypeCombobox.IsEnabled = false;
            //non player is checked at the beginning
            playerChecked = false;
            shootButton.IsEnabled = false;
        }

        private void GoalTypeLoadedCombobox(object sender, RoutedEventArgs e)
        {
            // get the comboBox reference.
            var comboBox = sender as ComboBox;

            // combobox will show team list
            comboBox.ItemsSource = manager.GetGoalType();

        }

        private void GoalTypeSelChangedCombobox(object sender, RoutedEventArgs e)
        {

            checkShootButton();
        }

        private void goalTypeChecked(object sender, RoutedEventArgs e)
        {
            GoalTypeCombobox.IsEnabled = true;
            //GoalTypeCombobox.IsEnabled = true;
            playerChecked = true;
            checkShootButton();
            GoalTypeCombobox.SelectedIndex = -1;
            Player pl = (Player)(sender as RadioButton).DataContext;
            shootingPlayer = pl;
        }

        private void GoalTypeComboboxDropDownClosed(object sender, EventArgs e)
        {

        }

        ///Checks if shoot button is
        private void checkShootButton()
        {
            // if player and goal type is selected
            if (GoalTypeCombobox.SelectedIndex > -1 && playerChecked == true)
            {
                shootButton.IsEnabled = true;
            }
        }

        private void shootButtonClick(object sender, RoutedEventArgs e)
        {
            done = true;
            updateMatch();
            Close();

        }
        private void updateMatch()
        {
            var selItem = (GoalType)(GoalTypeCombobox.SelectedItem);
            int teamId;
            
            //in which team is the player
            teamId = shootingPlayer.team_fk;

            // update database
            manager.updateScore(matchId, teamId, selItem.type, shootingPlayer.id);

            manager.updateGoal(shootingPlayer.id, teamId, matchId, selItem.id);
        }

        private void goalTypeCheckboxCLicked(object sender, EventArgs e)
        {

        }
    }
}
