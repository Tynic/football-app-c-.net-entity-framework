﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Console
{
    public class PlayerViewModel
    {
        public string Name { get; set; }
        public string Surname { get; set; }

        public int Goals { get; set; }
        public string Team { get; set; }
        public string Position { get; set; }
        public int Matches { get; set; }
    }
}
