USE [studenti]
GO
ALTER TABLE [dbo].[Roster] DROP CONSTRAINT [player_fk_s]
GO
ALTER TABLE [dbo].[Roster] DROP CONSTRAINT [match_fk_s]
GO
ALTER TABLE [dbo].[Player] DROP CONSTRAINT [team_fk]
GO
ALTER TABLE [dbo].[Player] DROP CONSTRAINT [position_fk]
GO
ALTER TABLE [dbo].[Match] DROP CONSTRAINT [home_team_fk]
GO
ALTER TABLE [dbo].[Match] DROP CONSTRAINT [guest_team_fk]
GO
ALTER TABLE [dbo].[Goal] DROP CONSTRAINT [player_fk]
GO
ALTER TABLE [dbo].[Goal] DROP CONSTRAINT [match_fk]
GO
ALTER TABLE [dbo].[Goal] DROP CONSTRAINT [goal_type_fk]
GO
ALTER TABLE [dbo].[Team] DROP CONSTRAINT [DF_Team_points]
GO
ALTER TABLE [dbo].[Player] DROP CONSTRAINT [DF_Player_goals_total]
GO
ALTER TABLE [dbo].[Match] DROP CONSTRAINT [DF_Match_home_score]
GO
/****** Object:  Table [dbo].[Team]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Team]
GO
/****** Object:  Table [dbo].[Roster]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Roster]
GO
/****** Object:  Table [dbo].[Position]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Position]
GO
/****** Object:  Table [dbo].[Player]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Player]
GO
/****** Object:  Table [dbo].[Match]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Match]
GO
/****** Object:  Table [dbo].[GoalType]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[GoalType]
GO
/****** Object:  Table [dbo].[Goal]    Script Date: 10.06.2016 19:09:56 ******/
DROP TABLE [dbo].[Goal]
GO
/****** Object:  Table [dbo].[Goal]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Goal](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[time] [datetime] NOT NULL,
	[player_fk] [int] NOT NULL,
	[match_fk] [int] NOT NULL,
	[goal_type_fk] [int] NOT NULL,
 CONSTRAINT [PK_Goal] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GoalType]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GoalType](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[type] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_GoalType] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Match]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Match](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[home_team_fk] [int] NOT NULL,
	[home_score] [int] NULL,
	[guest_score] [int] NULL,
	[start_time] [datetime] NULL,
	[end_time] [datetime] NULL,
	[guest_team_fk] [int] NOT NULL,
 CONSTRAINT [PK_Match] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Player]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Player](
	[id] [int] NOT NULL,
	[name] [nvarchar](50) NOT NULL,
	[surname] [nvarchar](50) NOT NULL,
	[goals_total] [int] NULL,
	[team_fk] [int] NOT NULL,
	[position_fk] [int] NOT NULL,
 CONSTRAINT [PK_Player] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Position]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Position](
	[id] [int] NOT NULL,
	[position] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Position] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Roster]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Roster](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[match_fk] [int] NOT NULL,
	[player_fk] [int] NOT NULL,
 CONSTRAINT [PK_Roster] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Team]    Script Date: 10.06.2016 19:09:56 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Team](
	[id] [int] NOT NULL,
	[team_name] [nvarchar](50) NOT NULL,
	[points] [int] NULL,
 CONSTRAINT [PK_Team] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[GoalType] ON 

INSERT [dbo].[GoalType] ([id], [type]) VALUES (1, N'Goal')
INSERT [dbo].[GoalType] ([id], [type]) VALUES (2, N'Penalty')
INSERT [dbo].[GoalType] ([id], [type]) VALUES (3, N'Own goal')
SET IDENTITY_INSERT [dbo].[GoalType] OFF
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (1, N'Jerry', N'Shoe', 0, 5, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (2, N'Victor', N'Cabadaj', 0, 5, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (3, N'Cameron', N'Stars', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (4, N'Ole', N'Oak', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (5, N'Peter', N'Bonn', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (6, N'Francesco', N'Goya', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (7, N'Ian', N'William', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (8, N'Ian', N'Sock', 0, 5, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (9, N'John', N'Taylor', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (10, N'Kayne', N'West', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (11, N'Clint', N'Eastwood', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (12, N'Jeremy', N'Sinn', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (13, N'Loyd', N'Weber', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (14, N'Quido', N'Jovi', 0, 5, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (15, N'Kurt', N'Nilson', 0, 4, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (16, N'Kurt', N'Cobain', 0, 4, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (17, N'Marshall', N'Malinowski', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (18, N'John', N'Doe', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (19, N'Guy', N'Gogh', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (20, N'Xavier', N'Nilson', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (21, N'Stanley', N'Evans', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (22, N'Finn', N'Evans', 0, 4, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (23, N'Daniel', N'Sock', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (24, N'Will', N'Skull', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (25, N'Paul', N'Fayet', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (26, N'Dave', N'Wall', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (27, N'Jarvis', N'Watson', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (28, N'James', N'Watson', 0, 4, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (29, N'Hermes', N'Evan', 0, 3, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (30, N'James', N'Levis', 0, 3, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (31, N'Hans', N'Andersen', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (32, N'Steve', N'Jobs', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (33, N'Rock', N'Rolland', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (34, N'Arnold', N'Ritchie', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (35, N'Tony', N'Lass', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (36, N'Lado', N'Levis', 0, 3, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (37, N'Stanley', N'Loyd', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (38, N'Willi', N'Wonka', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (39, N'Ken', N'Mass', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (40, N'Kill', N'Bill', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (41, N'DJ', N'Bobo', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (42, N'Bon', N'Jovi', 0, 3, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (43, N'Jan', N'Tong', 0, 2, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (44, N'Hogo', N'Fogo', 0, 2, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (45, N'Kal', N'Penn', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (46, N'Louis', N'Dock', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (47, N'Yan', N'Tang', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (48, N'Hugh', N'Grant', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (49, N'Thomas', N'Neumann', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (50, N'Arthur', N'Weiss', 0, 2, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (51, N'Tim', N'Burton', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (52, N'Tony', N'Jeffersonn', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (53, N'Arnold', N'Dark', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (54, N'Bonn', N'Sun', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (55, N'Hans', N'Robinson', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (56, N'Nick', N'Willow', 0, 2, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (57, N'Johan', N'White', 0, 1, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (58, N'Olaf', N'Peterson', 0, 1, 1)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (59, N'Michael', N'Rock', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (60, N'Will', N'Kiss', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (61, N'Hoid', N'Turing', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (62, N'John', N'Wilde', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (63, N'Jimmy', N'Sun', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (64, N'Lucas', N'Bayer', 0, 1, 2)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (65, N'Peter', N'Hill', 0, 1, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (66, N'Peter', N'Ostende', 0, 1, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (67, N'Simon', N'Dock', 0, 1, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (68, N'Don', N'Seladon', 0, 1, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (69, N'Gunter', N'Grey', 0, 1, 3)
INSERT [dbo].[Player] ([id], [name], [surname], [goals_total], [team_fk], [position_fk]) VALUES (70, N'Alan', N'Poe', 0, 1, 3)
INSERT [dbo].[Position] ([id], [position]) VALUES (1, N'Goalkeeper')
INSERT [dbo].[Position] ([id], [position]) VALUES (2, N'Defender')
INSERT [dbo].[Position] ([id], [position]) VALUES (3, N'Forward')
INSERT [dbo].[Team] ([id], [team_name], [points]) VALUES (1, N'Bears', 0)
INSERT [dbo].[Team] ([id], [team_name], [points]) VALUES (2, N'Foxes', 0)
INSERT [dbo].[Team] ([id], [team_name], [points]) VALUES (3, N'Rabbits', 0)
INSERT [dbo].[Team] ([id], [team_name], [points]) VALUES (4, N'Snakes', 0)
INSERT [dbo].[Team] ([id], [team_name], [points]) VALUES (5, N'Zebras', 0)
ALTER TABLE [dbo].[Match] ADD  CONSTRAINT [DF_Match_home_score]  DEFAULT ((0)) FOR [home_score]
GO
ALTER TABLE [dbo].[Player] ADD  CONSTRAINT [DF_Player_goals_total]  DEFAULT ((0)) FOR [goals_total]
GO
ALTER TABLE [dbo].[Team] ADD  CONSTRAINT [DF_Team_points]  DEFAULT ((0)) FOR [points]
GO
ALTER TABLE [dbo].[Goal]  WITH CHECK ADD  CONSTRAINT [goal_type_fk] FOREIGN KEY([goal_type_fk])
REFERENCES [dbo].[GoalType] ([id])
GO
ALTER TABLE [dbo].[Goal] CHECK CONSTRAINT [goal_type_fk]
GO
ALTER TABLE [dbo].[Goal]  WITH CHECK ADD  CONSTRAINT [match_fk] FOREIGN KEY([match_fk])
REFERENCES [dbo].[Match] ([id])
GO
ALTER TABLE [dbo].[Goal] CHECK CONSTRAINT [match_fk]
GO
ALTER TABLE [dbo].[Goal]  WITH CHECK ADD  CONSTRAINT [player_fk] FOREIGN KEY([player_fk])
REFERENCES [dbo].[Player] ([id])
GO
ALTER TABLE [dbo].[Goal] CHECK CONSTRAINT [player_fk]
GO
ALTER TABLE [dbo].[Match]  WITH CHECK ADD  CONSTRAINT [guest_team_fk] FOREIGN KEY([home_team_fk])
REFERENCES [dbo].[Team] ([id])
GO
ALTER TABLE [dbo].[Match] CHECK CONSTRAINT [guest_team_fk]
GO
ALTER TABLE [dbo].[Match]  WITH CHECK ADD  CONSTRAINT [home_team_fk] FOREIGN KEY([home_team_fk])
REFERENCES [dbo].[Team] ([id])
GO
ALTER TABLE [dbo].[Match] CHECK CONSTRAINT [home_team_fk]
GO
ALTER TABLE [dbo].[Player]  WITH CHECK ADD  CONSTRAINT [position_fk] FOREIGN KEY([position_fk])
REFERENCES [dbo].[Position] ([id])
GO
ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [position_fk]
GO
ALTER TABLE [dbo].[Player]  WITH CHECK ADD  CONSTRAINT [team_fk] FOREIGN KEY([team_fk])
REFERENCES [dbo].[Team] ([id])
GO
ALTER TABLE [dbo].[Player] CHECK CONSTRAINT [team_fk]
GO
ALTER TABLE [dbo].[Roster]  WITH CHECK ADD  CONSTRAINT [match_fk_s] FOREIGN KEY([match_fk])
REFERENCES [dbo].[Match] ([id])
GO
ALTER TABLE [dbo].[Roster] CHECK CONSTRAINT [match_fk_s]
GO
ALTER TABLE [dbo].[Roster]  WITH CHECK ADD  CONSTRAINT [player_fk_s] FOREIGN KEY([player_fk])
REFERENCES [dbo].[Player] ([id])
GO
ALTER TABLE [dbo].[Roster] CHECK CONSTRAINT [player_fk_s]
GO
